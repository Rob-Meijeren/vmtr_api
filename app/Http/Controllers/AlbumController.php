<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Album;
use Auth;

class AlbumController extends Controller {
	
	public function getAllAlbums(Request $request){
		$albums = Album::all();
		
		return json_encode(array('albums' => $albums));
	}

	public function getPublicAlbums(Request $request) {
	    $albums = Album::where('public', '=', '1')->get();

	    return json_encode(array('albums' => $albums));
	}

    public function markAsPublic(Request $request) {
        $album = Album::where('id', '=', $request->albumId)->first();

        $album->public = 1;
        $album->save();

        return json_encode(true);
    }

    public function markAsPrivate(Request $request) {
        $album = Album::where('id', '=', $request->albumId)->first();

        $album->public = 0;
        $album->save();

        return json_encode(true);
    }
	
	public function getSpecificAlbum($id){
		$album = Album::find($id);

		return json_encode(array('album' => $album));
	}
	
	public function addAlbum(Request $request){
		
		$rules = array(
				'albumName' => 'required',
		);
		
		$this->validate($request, $rules);
						
		$album = new Album();
		$album->name = $request->albumName;
		$album->description = $request->albumDescription;
		$album->public = 0;
		$album->save();

		return json_encode(array('result' => 'Het album is aangemaakt', 'albumID' => $album->id));
	}
	
	public function updateAlbum(Request $request){
		$albumID = $request->albumID;
		$albumNaam = $request->albumNaam;
		$albumOmschrijving = $request->albumOmschrijving;
		
		$album = Album::find($albumID);
		$album->Naam = $albumNaam;
		$album->Omschrijving = $albumOmschrijving;
		$album->save();
	}
	
	public function removeAlbum(Request $request){
		$albumID = $request->albumId;
		$album = Album::find($albumID);
		$album->delete();
	}

}