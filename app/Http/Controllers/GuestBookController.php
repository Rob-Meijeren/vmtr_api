<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Comment;
use Illuminate\Http\Request;

class GuestBookController extends Controller {	
	public function plaatsComment (Request $request){
		if($request->message !== ''){
			$newComment = new Comment();
			if($request->name != ''){
				$newComment->name = $request->name;
			}
			$newComment->comment = $request->message;
			$newComment->save();
		}

		return json_encode(array('newComment' => $newComment));
	}

	public function getAllComments () {
		$comments = Comment::orderBy('created_at', 'desc')->get()->toArray();

		return json_encode(array('comments' => $comments));
	}
}