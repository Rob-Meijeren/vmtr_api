<?php 

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use GuzzleHttp\Client;
use App\Models\User;
use App\Models\Maillist;
use Auth;
use Hash;
use DB;

class UserController extends Controller
{
    /**
     * Log a user in.
     *
     * @return Response
     */
    public function login(Request $request) {
        $rules = array(
                'username'=>'required|email',
                'password'=>'required|regex:/^[\pL\s\d\.]+$/u',
        );
    
        $this->validate($request, $rules);

        $client = new Client(['base_uri' => env('APP_URL')]);
        return $client->request('POST', 'oauth/token', ['form_params' => [
                                                                'grant_type' => 'password',
                                                                'client_id' => env('PASSWORD_CLIENT_ID'),
                                                                'client_secret' => env('PASSWORD_CLIENT_SECRET'),
                                                                'username' => $request->username,
                                                                'password' => $request->password,
                                                                'scope' => '*',
                                                            ],
                                                            'verify' => false])->getBody();
    }

    public function logout(Request $request) {
        $request->user()->token()->revoke();

        return response()->json(true);
    }

    /**
     * Get a user by the token from the header.
     *
     * @return Response
     */
    public function getUser(Request $request) {
        return response()->json($request->user());
    }


    public function updateAccount(Request $request) {
        $rules = array(
            'username'=>'required',
            'email'=>'required|email',
        );

        $this->validate($request, $rules);

        $user = $request->user();

        $user->name = $request->username;
        $user->email = $request->email;
        $user->save();

        return json_encode(array('result' => 'Uw account is geupdated'));
    }

    public function updatePassword(Request $request) {
        $rules = array(
            'oldPassword'=>'required|regex:/^[\w\W\s\d\.]+$/u',
            'password'=>'required|regex:/^[\w\W\s\d\.]+$/u',
        );

        $this->validate($request, $rules);

        $user = $request->user();

        if(Hash::check($request->oldPassword, $user->password)) {
            $user->password =  Hash::make($request->password);
            $user->save();

            return json_encode(array('result' => 'Uw wachtwoord is geupdated'));
        }
        else {
            return json_encode(array('error' => 'Uw huidige wachtwoord klopt niet'));
        }
    }

    public function register(Request $request) {
        $rules = array(
                'name'=>'required',
                'email'=>'required|email',
                'password'=>'required|regex:/^[\w\W\s\d\.]+$/u',
        );
        
        $this->validate($request, $rules);
        
        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->save();
        
        return json_encode(array('success' => true));
    }

    public function subscribeMaillist(Request $request) {
        $email = $request->email;
        $naam = $request->name;

        if(!empty($email) && !empty($naam)) {
            $existing = Maillist::where('email', '=', $email)->first();

            if(is_null($existing)) {
                $mail = new Maillist();
                $mail->name = $naam;
                $mail->email = $email;
                $mail->no_of_downloaded_routes = 1;
                $mail->save();

                return json_encode(array('success' => 'new'));
            }
            else {
                $existing->no_of_downloaded_routes = ($existing->no_of_downloaded_routes + 1);
                $existing->save();
                return json_encode(array('success' => 'existing'));
            }
        }
        else {
            return json_encode(array('error' => 'empty fields'));
        }
    }
}