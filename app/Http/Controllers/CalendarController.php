<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use DB;
use App\Models\Evenement;
use DateTime;

class CalendarController extends Controller {
	
	public function getUpcomingEvents(){
		$events = [];

		$date = date("Y-m-d");
		$eventArray = Evenement::where('start', '>=', $date)->orderBy('start', 'ASC')->get();
		
		foreach($eventArray as $event){
			array_push($events, $this->processEvent($event));
		}
		
		return json_encode(array('events' => $events));
	}

	public function getNextEvent(){
		$date = date("Y-m-d");
		$firstEvent = Evenement::where('start' , '>', $date)->orderBy('start', 'ASC')->first();
		$nextEvent = null;
		
		if(isset($firstEvent)) {
			$nextEvent = $this->processEvent($firstEvent);
		}
		
		return json_encode(array('nextEvent' => $nextEvent));
	}

	public function getAllEvents() {
		$events = [];
		$eventRecords = Evenement::all();

		foreach($eventRecords as $event){
			array_push($events, $this->processEvent($event));
		}

		return json_encode(array('events' => $events));
	}

	public function updateEventDates(Request $request) {
		$eventId = $request->id;
		$startDate = $request->start;
		$endDate = $request->end;

		$event = Evenement::find($eventId);
		$event->start = $startDate;
		$event->end = $endDate;
		$event->save();
	}

	private function processEvent($event) {
		setlocale(LC_ALL, 'nl_NL');
		setlocale(LC_TIME, 'Dutch');

		$arr = [];
		if ($event["id"]) {
			$arr["id"] = $event["id"];
		}
		$arr["title"] = $event["title"];
		
		$oDate = new DateTime($event["start"]);
		$arr["start"] = $oDate->format("d-m-Y");

		$oDate = new DateTime($event["end"]);
		$arr["end"] = $oDate->format("d-m-Y");
		
		$timestamp = $oDate->getTimestamp();
		$arr["dayNumber"] = strftime('%d', $timestamp);
		$arr["day"] = strftime('%B', $timestamp);
		$arr["month"] =  strftime('%A', $timestamp);
		$arr["description"] = $event["description"];
		$arr["membersOnly"] = $event["membersOnly"];

		return $arr;
	}
}