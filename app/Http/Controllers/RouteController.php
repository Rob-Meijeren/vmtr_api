<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Storage;
use App\Models\Route;

class RouteController extends Controller {

	public function getAllRoutes(Request $request) {
		$routeNames = array();

        $routes = Route::all()->toArray();

		return json_encode(array('routes' => $routes));
	}

	public function getPublicRoutes(Request $request) {
        $routeNames = array();

        $routes = Route::where('public', '=', '1')->get()->toArray();

        return json_encode(array('routes' => $routes));
	}

	public function getSpecificRoute(Request $request) {
        $route = Route::where('filename', '=', $request->routeName)->get()->toArray();

        return json_encode(array('route' => $route));
    }

	public function downloadGPX(Request $request) {
		$filename = $request->routeName .'.gpx';

		if(Storage::disk('gpx')->has($filename)){
			return response()->download(storage_path('routes/gpx/'. $filename));
		}
		else {
			return false;
		}
	}

	public function downloadKML(Request $request) {
		$filename = $request->routeName . '.kml';

		if(Storage::disk('kml')->has($filename)){
			return response()->download(storage_path('routes/kml/'. $filename));
		}
		else {
			return false;
		}
	}

	public function downloadITN(Request $request) {
        $filename = $request->routeName . '.itn';

        if(Storage::disk('itn')->has($filename)){
            return response()->download(storage_path('routes/itn/'. $filename));
        }
        else {
            return false;
        }
    }

    public function checkFilePresence(Request $request) {
        $presentArray = [ 'gpx' => Storage::disk('gpx')->has($request->routeName . '.gpx'),
                          'kml' => Storage::disk('kml')->has($request->routeName . '.kml'),
                          'itn' => Storage::disk('itn')->has($request->routeName . '.itn')];
        return json_encode(array('present' => $presentArray));
    }

    public function markAsPublic(Request $request) {
        $route = Route::where('filename', '=', $request->filename)->first();

        $route->public = 1;
        $route->save();

        return json_encode(array('success' => 'true'));
    }

    public function markAsPrivate(Request $request) {
        $route = Route::where('filename', '=', $request->filename)->first();

        $route->public = 0;
        $route->save();

        return json_encode(array('success' => 'true'));
    }

	public function uploadRoute(Request $request) {
		$route = $request->route;

		$file = base64_decode($route['base64_string'], true);

		if($file) {
            $filename = $route['filename'];
            $name = substr($filename, 0 , strlen($filename)-4);
            $underscored_name = str_replace(' ', '_', $name);
            $extension = substr($filename, -3, 3);

            $existing_route = Route::where('filename', '=', $underscored_name)->first();

            if(is_null($existing_route)) {
                $routePoints = $this->getPointsOfRoute($file, $extension);

                $kml = $this->createKML($routePoints, $underscored_name);
                $gpx = $this->createGPX($routePoints, $underscored_name);
                $itn = $this->createITN($routePoints, $underscored_name);

                $centerCoordinates = $this->getCenterOfRoute($routePoints->waypoints);

                $dbRoute = new Route();
                $dbRoute->name = $name;
                $dbRoute->filename = $underscored_name;
                $dbRoute->centerLatitude = $centerCoordinates['latitude'];
                $dbRoute->centerLongitude = $centerCoordinates['longitude'];
                $dbRoute->public = 0;
                $dbRoute->save();

                return response()->json($dbRoute);
            } else {
                return json_encode(array('route' => 'duplicate'));
            }
        } else {
            return json_encode(array('error' => 'Er is iets misgegaan met het versturen van het bestand. Probeer het nogmaals.'));
        }
	}

	private function getPointsOfRoute($route, $extension) {
	    $pointArray = new \stdClass();
	    $pointArray->waypoints = [];
	    $pointArray->track = [];

	    if($extension === 'gpx') {
	        $route = utf8_encode($route);
	        $route = simplexml_load_string($route);

            foreach($route->rte->rtept as $key => $point) {
                $object = new \stdClass();
                $object->name = $point->name;
                $object->latitude = $point['lat'];
                $object->longitude = $point['lon'];
                $object->elevation = 0;
                array_push($pointArray->waypoints, $object);
            }
            if($route->trk) {
                $trackName = $route->trk->name;
                foreach($route->trk->trkseg->trkpt as $key => $point) {
                    $object = new \stdClass();
                    $object->name = $trackName;
                    $object->latitude = $point['lat'];
                    $object->longitude = $point['lon'];
                    $object->elevation = $point->ele;
                    array_push($pointArray->track, $object);
                }
            } else {
                $pointArray->track = $pointArray->waypoints;
            }
	    }
	    else if($extension === 'kml') {
	        $route = simplexml_load_string($route);
	        foreach($route->Document->Folder[0]->Placemark as $key => $point) {
	            $object = new \stdClass();
                $object->name = $point->name;
                $object->latitude = $point->LookAt->latitude;
                $object->longitude = $point->LookAt->longitude;
                $object->elevation = $point->LookAt->tilt;
	            array_push($pointArray->waypoints, $object);
	        }
	        $track = $route->Document->Folder[1];
	        $name = $track->name;
	        $trackPoints = explode(' ', $track->Placemark->MultiGeometry->LineString->coordinates);
	        foreach($trackPoints as $point) {
	            $object = new \stdClass();
                $object->name = $name;
                $latlong = explode(',', $point);
                $object->latitude = $latlong[1];
                $object->longitude = $latlong[0];
                $object->elevation = $latlong[2];
                array_push($pointArray->track, $object);
	        }
	    }
	    else if($extension === 'itn') {
	        $route = preg_split('/\n|\r\n?/', $route);
	        foreach($route as $key => $point) {
	            if($key !== (count($route) - 1)) {
                    list($lon, $lat, $name) = explode('|', $point);
                    $object = new \stdClass();
                    $object->name = $name;
                    $object->latitude = $lat;
                    $object->longitude = $lon;
                    array_push($pointArray, $object);
                }
            }
	    }
        return $pointArray;
	}

	private function createKML($routePoints, $filename) {
        $dom = new \DOMDocument('1.0', 'UTF-8');

        // Creates the root KML element and appends it to the root document.
        $node = $dom->createElementNS('http://earth.google.com/kml/2.1', 'kml');
        $parNode = $dom->appendChild($node);

        // Creates a KML Document element and append it to the KML element.
        $dnode = $dom->createElement('Document');
        $docNode = $parNode->appendChild($dnode);

        $visnode = $dom->createElement('visibility',1);
        $docNode->appendChild($visnode);

        $wayPointFolderNode = $dom->createElement('Folder');
        $docNode->appendChild($wayPointFolderNode);

        $visnode = $dom->createElement('visibility',1);
        $wayPointFolderNode->appendChild($visnode);

        foreach ($routePoints->waypoints as $key => $value) {
          // Creates a Placemark and append it to the Document.

          $placemarkNode = $dom->createElement('Placemark');
          $placeNode = $wayPointFolderNode->appendChild($placemarkNode);

          $placeNode->appendChild($visnode);

          // Create name, and description elements and assigns them the values of the name and address columns from the results.
          $nameNode = $dom->createElement('name', $value->name);
          $placeNode->appendChild($nameNode);

          $lookAtNode = $dom->createElement('LookAt');
          $placeNode->appendChild($lookAtNode);

          $longitudeNode = $dom->createElement('longitude', $value->longitude);
          $lookAtNode->appendChild($longitudeNode);
          $latitudeNode = $dom->createElement('latitude', $value->latitude);
          $lookAtNode->appendChild($latitudeNode);
          $rangeNode = $dom->createElement('range', 600);
          $lookAtNode->appendChild($rangeNode);
          $tiltNode = $dom->createElement('tilt', $value->elevation);
          $lookAtNode->appendChild($tiltNode);
          $headingNode = $dom->createElement('heading', 0);
          $lookAtNode->appendChild($headingNode);

          // Creates a Point element.
          $pointNode = $dom->createElement('Point');
          $placeNode->appendChild($pointNode);

          // Creates a coordinates element and gives it the value of the lng and lat columns from the results.
          $coorStr = $value->longitude . ','  . $value->latitude . ',' . $value->elevation;
          $coorNode = $dom->createElement('coordinates', $coorStr);
          $pointNode->appendChild($coorNode);
        }

        $trackFolderNode = $dom->createElement('Folder');
        $docNode->appendChild($trackFolderNode);

        $trackName = $dom->createElement('name', $filename);
        $trackFolderNode->appendChild($trackName);
        $trackFolderNode->appendChild($visnode);

        $trackPlacemarkNode = $dom->createElement('Placemark');
        $lastPointName = $dom->createElement('name', $routePoints->waypoints[count($routePoints->waypoints) - 1]->name);
        $trackPlacemarkNode->appendChild($lastPointName);
        $trackPlacemarkNode->appendChild($visnode);

        $multiGeometryNode = $dom->createElement('MultiGeometry');
        $lineStringNode = $dom->createElement('LineString');
        $coordinatesString = "";
        foreach($routePoints->track as $key => $value) {
            $coordinatesString .= $value->longitude . ',' . $value->latitude . ',' . $value->elevation . ' ';
        }
        $lineStringCoordinates = $dom->createElement('coordinates', $coordinatesString);
        $lineStringNode->appendChild($lineStringCoordinates);
        $multiGeometryNode->appendChild($lineStringNode);
        $trackPlacemarkNode->appendChild($multiGeometryNode);
        $trackFolderNode->appendChild($trackPlacemarkNode);

        if(Storage::disk('kml')->put($filename . '.kml', $dom->saveXML())) {
            return true;
        }
        else {
            return false;
        }
	}

	private function createGPX($routePoints, $filename) {
	    $dom = new \DOMDocument('1.0', 'UTF-8');

        // Creates the root GPX element and appends it to the root document.
        $node = $dom->createElementNS('http://www.topografix.com/GPX/1/1', 'gpx');
        $version = $dom->createAttribute('version');
        $version->value = '1.1';
        $node->appendChild($version);
        $creator = $dom->createAttribute('creator');
        $creator->value = 'VMTR Norg (www.vmtr.nl)';
        $node->appendChild($creator);
        $schema = $dom->createAttribute('xsi:schemaLocation');
        $schema->value = 'http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd http://www.garmin.com/xmlschemas/WaypointExtension/v1 http://www8.garmin.com/xmlschemas/WaypointExtensionv1.xsd http://www.garmin.com/xmlschemas/TrackPointExtension/v1 http://www.garmin.com/xmlschemas/TrackPointExtensionv1.xsd http://www.garmin.com/xmlschemas/GpxExtensions/v3 http://www8.garmin.com/xmlschemas/GpxExtensionsv3.xsd http://www.garmin.com/xmlschemas/ActivityExtension/v1 http://www8.garmin.com/xmlschemas/ActivityExtensionv1.xsd http://www.garmin.com/xmlschemas/AdventuresExtensions/v1 http://www8.garmin.com/xmlschemas/AdventuresExtensionv1.xsd';
        $node->appendChild($schema);
        $xsiAttr = $dom->createAttribute('xmlns:xsi');
        $xsiAttr->value = 'http://www.w3.org/2001/XMLSchema-instance';
        $node->appendChild($xsiAttr);
        $wptx1Attr = $dom->createAttribute('xmlns:wptx1');
        $wptx1Attr->value = 'http://www.garmin.com/xmlschemas/WaypointExtension/v1';
        $node->appendChild($wptx1Attr);
        $gpxtrx = $dom->createAttribute('xmlns:gpxtrx');
        $gpxtrx->value = 'http://www.garmin.com/xmlschemas/GpxExtensions/v3';
        $node->appendChild($gpxtrx);
        $gpxtpx = $dom->createAttribute('xmlns:gpxtpx');
        $gpxtpx->value = 'http://www.garmin.com/xmlschemas/TrackPointExtension/v1';
        $node->appendChild($gpxtpx);
        $gpxxAttr = $dom->createAttribute('xmlns:gpxx');
        $gpxxAttr->value = 'http://www.garmin.com/xmlschemas/GpxExtensions/v3';
        $node->appendChild($gpxxAttr);
        $trpAttr = $dom->createAttribute('xmlns:trp');
        $trpAttr->value = 'http://www.garmin.com/xmlschemas/TripExtensions/v1';
        $node->appendChild($trpAttr);
        $advAttr = $dom->createAttribute('xmlns:adv');
        $advAttr->value = 'http://www.garmin.com/xmlschemas/AdventuresExtensions/v1';
        $node->appendChild($advAttr);
        $parNode = $dom->appendChild($node);

        $metadata = $dom->createElement('metadata');
        $metaNode = $parNode->appendChild($metadata);
        $currentTime = new \DateTime();
        $timeNode = $dom->createElement('time', $currentTime->format('Y-m-d\TH:i:s\Z'));
        $metaNode->appendChild($timeNode);

        $rteNode = $dom->createElement('rte');
        $rte = $parNode->appendChild($rteNode);

        $nameNode = $dom->createElement('name', 'Route-' . $filename);
        $rte->appendChild($nameNode);

        foreach ($routePoints->waypoints as $key => $value) {
          // Creates a Placemark and append it to the Document.

          $rteptNode = $dom->createElement('rtept');

          $longitudeAttr = $dom->createAttribute('lon');
          $longitudeAttr->value = $value->longitude;
          $rteptNode->appendChild($longitudeAttr);
          $latitudeAttr = $dom->createAttribute('lat');
          $latitudeAttr->value = $value->latitude;
          $rteptNode->appendChild($latitudeAttr);

          $rteptNameNode = $dom->createElement('name', $value->name);
          $rteptNode->appendChild($rteptNameNode);

          $extensionsNode = $dom->createElement('extensions');

          if($key === 0 || $key === (count($routePoints) -1)) {
            $viaPoint = $dom->createElement('trp:ViaPoint');
            $extensionsNode->appendChild($viaPoint);
          }
          else {
            $shapingPoint = $dom->createElement('trp:ShapingPoint');
            $extensionsNode->appendChild($shapingPoint);
          }

          $routePointExtension = $dom->createElement('gpxx:RoutePointExtension');
          $subClass = $dom->createElement('gpxx:Subclass', '000000000000FFFFFFFFFFFFFFFFFFFFFFFF');
          $routePointExtension->appendChild($subClass);
          $extensionsNode->appendChild($routePointExtension);

          $rteptNode->appendChild($extensionsNode);

          $rte->appendChild($rteptNode);
        }

        $trkNode = $dom->createElement('trk');
        $parNode->appendChild($trkNode);

        $nameNode = $dom->createElement('name', 'Track-' . $filename);
        $trkNode->appendChild($nameNode);

        $trksegNode = $dom->createElement('trkseg');
        $trkNode->appendChild($trksegNode);

        foreach($routePoints->track as $key => $value) {
            $trkpt = $dom->createElement('trkpt');

            $lonAttr = $dom->createAttribute('lon');
            $lonAttr->value = $value->longitude;
            $trkpt->appendChild($lonAttr);

            $latAttr = $dom->createAttribute('lat');
            $latAttr->value = $value->latitude;
            $trkpt->appendChild($latAttr);

            $eleNode = $dom->createElement('ele', $value->elevation);
            $trkpt->appendChild($eleNode);
            $trksegNode->appendChild($trkpt);
        }

        if(Storage::disk('gpx')->put($filename . '.gpx', $dom->saveXML())) {
            return true;
        }
        else {
            return false;
        }
	}

	private function createITN($routePoints, $filename) {
	    $fileString = "";

	    foreach($routePoints->waypoints as $key => $value) {
	        $formattedLongitude = substr(str_replace('.', '', $value->longitude), 0, 7);
	        $formattedLatitude = substr(str_replace('.', '', $value->latitude), 0, 7);
	        $fileString .= $formattedLongitude . '|'. $formattedLatitude . '|' . $value->name;

	        if($key === 0) {
	            $fileString .= '|4|';
	        }
	        else if ($key === (count($routePoints->waypoints) - 1)) {
	            $fileString .= '|2|';
	        }
	        else {
	            $fileString .= '|0|';
	        }

	        $fileString .= PHP_EOL;
	    }

	    $fileString .= '""';

	    if(Storage::disk('itn')->put($filename . '.itn', $fileString)) {
            return true;
        }
        else {
            return false;
        }
	}

	private function GetCenterOfRoute($routePoints) {
		$radialValues = [];

		$avgLat = 0.0;
        $avgLon = 0.0;

        foreach ($routePoints as $key => $value) {
            $tmpLat = (double) $value->latitude;
            $tmpLon = (double) $value->longitude;

            $avgLat += $tmpLat;
            $avgLon += $tmpLon;
        }

        $avgLat /= count($routePoints);
        $avgLon /= count($routePoints);

        return array("latitude" => $avgLat, "longitude" => $avgLon);
	}
}