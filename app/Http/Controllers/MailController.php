<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Mail;

class MailController extends Controller {
	public function postLidWorden(Request $request){
		$rules = array(
				'name' => 'required|regex:/^[\pL\s\d\.]+$/u',
				'email' => 'required|email'
		);

		$this->validate($request, $rules);

		Mail::send('emails.nieuwLid', array('name' => $request->name, 'phone' => $request->phone, 'email' => $request->email), function($message) {
			$message->to('vmtrnorg@outlook.com')->subject('Nieuw Lid');
			$message->from('webmaster@vmtr.nl');
		});
	}

	public function sendMessage(Request $request){
		$rules = array(
					'name' => 'required|regex:/^[\pL\s\d\.]+$/u',
				    'email' => 'required|email',
				    'subject' => 'required|regex:/^[\pL\s\d\.]+$/u',
				    'message' => 'required|regex:/^[\pL\s\d\.]+$/u'
				    );

		$this->validate($request, $rules);

	    Mail::send('emails.contactForm', array('name' => $request->name, 'phone' => $request->phone, 'email' => $request->email, 'bericht' => $request->message), function($message) {
	    	$message->to('info@vmtr.nl')->subject('Contact');
	    	$message->from('webmaster@vmtr.nl');
	    });
	}

	public function postTent(Request $request) {
		$rules = array(
					'name' => 'required|regex:/^[\pL\s\d\.]+$/u',
				    'email' => 'required|email',
				    'message' => 'required|regex:/^[\pL\s\d\.]+$/u'
				    );

		$this->validate($request, $rules);

	    Mail::send('emails.contactForm', array('name' => $request->name, 'phone' => $request->phone, 'email' => $request->email, 'bericht' => $request->message), function($message) use ($request) {
	    	$message->to('geessienus@home.nl')->subject('Aanvraag tenthuur');
	    });
	}
}
