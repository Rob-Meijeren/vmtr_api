<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Image;
use App\Models\Foto;

class PhotoController extends Controller {

	public function getPhotosOfAlbum($albumId) {
		$photos = Foto::where('album_id', '=', $albumId)->get();
		
		return json_encode(array('photos' => $photos->toArray()));
	}
	
	public function uploadPhoto(Request $request){
		$albumID = $request->albumId;
		$photo = $request->picture;
		
		$directory = public_path() . '/img/'. $albumID;

		if (!file_exists($directory)) {
		    mkdir($directory);
		}

		$img = Image::make($photo['base64_string']);
		$img->filename = $photo['filename'];

		$photo = new Foto();
		$photo->album_id = $albumID;
		$photo->path = '/img/'. $albumID . '/' . $img->filename;
		$photo->save();
		$img->save($directory . '/' . $img->filename);

		return json_encode(array('photo' => $img->filename));
	}
	
	public function removePhoto(Request $request){
		$photoID = $request->photoId;
		$photo = Foto::find($photoID);
		$photo->delete();
	}

}