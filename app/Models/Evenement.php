<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Evenement extends Model {

	protected $table = 'events';
	
	protected $hidden = array('id', 'created_at', 'updated_at');

}