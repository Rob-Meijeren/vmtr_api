<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'photo'], function () {
    Route::get('/specific/{albumId}', 'PhotoController@getPhotosOfAlbum');
});

Route::group(['prefix' => 'album'], function () {
    Route::get('public', 'AlbumController@getPublicAlbums');
    Route::get('/specific/{albumId}', 'AlbumController@getSpecificAlbum');
});

Route::group(['prefix' => 'event'], function () {
    Route::get('nextEvent', 'CalendarController@getNextEvent');
    Route::get('upcoming', 'CalendarController@getUpcomingEvents');
    Route::get('allEvents', 'CalendarController@getAllEvents');
});

Route::group(['prefix' => 'guestbook'], function () {
    Route::get('getAllComments', 'GuestBookController@getAllComments');
    Route::post('comment', 'GuestBookController@plaatsComment');
});

Route::group(['prefix' => 'contact'], function () {
    Route::post('signup', 'MailController@postLidWorden');
    Route::post('tent', 'MailController@postTent');
    Route::post('contact', 'MailController@sendMessage');
});

Route::group(['prefix' => 'route'], function () {
    Route::get('public', 'RouteController@getPublicRoutes');
    Route::get('specific/{routeName}', 'RouteController@getSpecificRoute');
    Route::get('downloadGPX/{routeName}', 'RouteController@downloadGPX');
    Route::get('downloadKML/{routeName}', 'RouteController@downloadKML');
    Route::get('downloadITN/{routeName}', 'RouteController@downloadITN');
    Route::get('checkFilePresence/{routeName}', 'RouteController@checkFilePresence');
    Route::post('subscribeMaillist', 'UserController@subscribeMaillist');
});

Route::group(['prefix' => 'user'], function () {
    Route::post('login', 'UserController@login');
    Route::post('create', 'UserController@register');
});

Route::group(['middleware' => 'auth:api'], function () {

	Route::group(['prefix' => 'photo'], function () {
		Route::post('remove', 'PhotoController@removePhoto');
		Route::post('upload', 'PhotoController@uploadPhoto');
	});

	Route::group(['prefix' => 'album'], function () {
	    Route::get('all', 'AlbumController@getAllAlbums');
	    Route::post('mark/private', 'AlbumController@markAsPrivate');
	    Route::post('mark/public', 'AlbumController@markAsPublic');
   		Route::post('remove', 'AlbumController@removeAlbum');
    	Route::post('update', 'AlbumController@updateAlbum');
    	Route::post('create', 'AlbumController@addAlbum');
	});

	Route::group(['prefix' => 'user'], function () {
		Route::get('user', 'UserController@getUser');
		Route::post('update', 'UserController@updateAccount');
		Route::post('updatePassword', 'UserController@updatePassword');
		Route::get('logout', 'UserController@logout');
	});

	Route::group(['prefix' => 'route'], function () {
	    Route::get('all', 'RouteController@getAllRoutes');
    	Route::post('upload', 'RouteController@uploadRoute');
	    Route::post('mark/private', 'RouteController@markAsPrivate');
	    Route::post('mark/public', 'RouteController@markAsPublic');
	});

	Route::group(['prefix' => 'event'], function () {
		Route::get('allEvents', 'CalendarController@getAllEvents');
		Route::post('update/dates', 'CalendarController@updateEventDates');
	}); 
});